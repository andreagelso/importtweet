var countFiles = require('count-files')
var fs = require('fs');
var crud = require('./elasticfunctions.js');
var SETTINGS = require('./config');
var sentiment = require('multilang-sentiment'); 
var tweets = Array();
var count=0;

crud.GetTweets( (err,resp)=> {

    if (err) throw err; 
    console.log(resp.hits.total);    
    
    resp.hits.hits.forEach(element => {
        var x = { multilangsentiment: sentiment(element._source.full_text).score};

        tweets.push({ _id :  element._id ,element : x});
        count++
        console.log({ _id :  element._id ,element : x});
        
        //last element bulk update
        if (count == resp.hits.total) {
            console.log("last element");
            
            crud.UpdateTweets(tweets, (err, resp, status)=> {
                if (err) throw err;
                console.log("Tweet updated: " + resp.items.length);
            });
        }

    });
});