var countFiles = require('count-files')
var fs = require('fs');
var crud = require('./elasticfunctions.js');
var SETTINGS = require('./config');
var mainFolder= './tweets_local';

function run(numfiles,callback) {
    
    var tweets=[];
    var count = 0;
     crud.deleteLocalTweet(() => {   
           
        fs.readdir(mainFolder, (err, cities) => {         
            if (err) throw err;

            cities.forEach(city => {
                //console.log(city)
                
                fs.readdir(mainFolder  + "/" + city, (err, days) => {
                   
                    days.forEach(day => {

                        fs.readdir(mainFolder  + "/" + city + "/" + day,(err, files) => {
                            
                            files.forEach(file => {
                                file = mainFolder  + "/" + city + "/" + day + "/" + file
                                
                                //console.log(file);
                                 
                                fs.readFile(file, 'utf8', function (err, contents) {
                                   if (err) throw err;
               
                                   try  {
                                       var tweet  = JSON.parse(contents);
                                       item = {
                                            id : tweet.id,
                                            created: day,
                                            city: city,
                                            type: "local",
                                            file : file,
                                            full_text : tweet.full_text,
                                            location : tweet.user.location, 
                                       }
                              
                                       tweets.push(item);

                                       count++       
                                                              
                                       if (count == numfiles) { //last element exit
                                           callback(tweets);
                                       }
                                        
                                   }catch(ex){
                                       console.log("=======ERROR=============");
                                       console.log(folder + " " + file);
                                   }
                                });
                                 
                                
                                 
                            });  
                             
                       });  
                        
                    });
    
                    
                    
                    
    
                });
                 
            });
          
        }); 
    })  
}

//GET NUMEBER OF FILES
countFiles(mainFolder, function (err, results) {
    if (err) throw err;

    var numfiles = results.files -1;
    console.log("File to import : " + numfiles);
    
    run(numfiles,tweets =>  {
        //BULK INSERT
         crud.InserTweets(tweets,  "local" , (err, resp, status)=> {
            if (err) throw err;
            console.log("Tweet inserted: " + resp.items.length);
        });  
     });
})