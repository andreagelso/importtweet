var countFiles = require('count-files')
var fs = require('fs');
var crud = require('./elasticfunctions.js');
var SETTINGS = require('./config');
var mainFolder = './tweets_global';

function run(numfiles,callback) {
    
    var tweets=[];
    var count = 0;
   crud.deleteGlobalTweet(() => {  
        
        fs.readdir(mainFolder, (err, days) => {         
            if (err) throw err;

            days.forEach(day => {
                
                fs.readdir(mainFolder  + "/" + day, (err, files) => {

                    files.forEach(file => { 

                        file =  mainFolder  + "/" + day + "/" + file;
                        
                        //console.log(file);
                        
                        fs.readFile(file, 'utf8', function (err, contents) {
                            if (err) throw err;
        
                            try  {
                                var tweet  = JSON.parse(contents);
                                item = {
                                    id : tweet.id,
                                     created: day,
                                     type: "global",
                                     file : file,
                                     full_text : tweet.full_text,
                                     location : tweet.user.location,
                                }
                        
                                tweets.push(item);

                                count++       
                        
                                if (count == numfiles) { //last element exit for
                                    callback(tweets);
                                }
                                
                            }catch(ex){
                                console.log("=======ERROR=============");
                                console.log(folder + " " + file);
                            }
                        });
                        
                    });  
                          
                             
                });  
                        
            });            
        });
        
     }); 
        
}

//GET NUMEBER OF FILES
countFiles(mainFolder, function (err, results) {
    if (err) throw err;

    var numfiles = results.files -1;
    console.log("File to import : " + numfiles);
    
    run(numfiles,tweets =>  {
        //BULK INSERT 
        crud.InserTweets(tweets, "global" , (err, resp, status)=> {
            if (err) throw err;
            console.log("Tweet inserted: " + resp.items.length);
        });
         
     });
})