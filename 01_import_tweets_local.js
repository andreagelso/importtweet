//LIBRARIES
var fs = require("fs")
var twitter = require('twitter');
var SETTINGS = require('./config');
var shell = require('shelljs');
var mainFolder= './tweets_located';

var client = new twitter(SETTINGS.twitterTokens);


function run(callback) {
    var days = [];
    var datetime;
    var day;
    
    for (i = 1; i <= SETTINGS.days; i++) {
        datetime = new Date();
        datetime.setDate(datetime.getDate() - i);
        day  = datetime.toISOString().substr(0, 10);
        item = { dd: day, hashtags: SETTINGS.hashtags }
        days.push(item);
    }

    return callback(days);
}


run((days) => { 
    var params;
    var arrayhashtag;
 


    //FOR EVERY DAY
    days.forEach(function (day) {

        arrayhashtag = day.hashtags.split(" ");
        
        //FOR EVERY CITY
        SETTINGS.cities.forEach(function (city) {

            //FOR EVERY HASTHTAG
            arrayhashtag.forEach(function (hashtag) {
                
                // TWITTER API CALL
                params = { q: hashtag, until: day.dd, lang: 'es', count: '100', result_type: 'recent', tweet_mode: 'extended', geocode: city.geocode , include_entities: true };
                
                //console.log(city.name)
                
                client.get('search/tweets', params, function (error, tweets, response) {
                    
                    if (error) { console.log(error); process.exit(1); }

                    var result = tweets.statuses;

                    //console.log(result);

                    try {
                        //FOR EVERY TWEET
                        result.forEach(element => {

                            //CREATE FOLDER
                            folder =  mainFolder + "/" + city.name + "/" + day.dd + "/";
                            if (!fs.existsSync(folder)) 
                                shell.mkdir('-p', folder);

                            //SAVE TWEET
                            file = folder + element.id + "_" + hashtag + ".json";                             
                            //console.log(file);
                            if (!fs.existsSync(file)) { 
                              
                                fs.writeFile(file, JSON.stringify(element), err => {
                                    if (err) throw err;
                                    console.log(file);
                                })
                            }
                        });

                    } catch (err) {
                        console.log(err);
                    }

                });

            });
        });

        

    });

});

