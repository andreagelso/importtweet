var countFiles = require('count-files')
var fs = require('fs');
var crud = require('./elasticfunctions.js');
var SETTINGS = require('./config');
var lorca = require('lorca-nlp');


function runLocal(numfiles,mainFolder,callback) {
    
    var tweets=[];
    var count = 0;
     crud.deleteLocalTweet(() => {   
           
        fs.readdir(mainFolder, (err, cities) => {         
            if (err) throw err;

            cities.forEach(city => {
                //console.log(city)
                
                fs.readdir(mainFolder  + "/" + city, (err, days) => {
                   
                    days.forEach(day => {

                        fs.readdir(mainFolder  + "/" + city + "/" + day,(err, files) => {
                            
                            files.forEach(file => {
                                file = mainFolder  + "/" + city + "/" + day + "/" + file
                                
                                //console.log(file);
                                 
                                fs.readFile(file, 'utf8', function (err, contents) {
                                   if (err) throw err;
               
                                   try  {
                                       var tweet  = JSON.parse(contents);
                                       var doc = lorca(tweet.full_text); // sentimental
                                        
                                        var sentiment; 
                                        if (doc.sentiment() == 0) 
                                            sentiment = "Neutral";
                                        else if (doc.sentiment() < 0) 
                                            sentiment = "Negative";
                                        else if (doc.sentiment() > 0) 
                                            sentiment = "Positive";
                                        
                                       

                                       item = {

                                            created: day,
                                            city: city,
                                            type: "local",
                                            file : file,
                                            full_text : tweet.full_text,
                                            location : tweet.user.location, 
                                            hashtags: tweet.entities.hashtags,
                                            lorca: doc.sentiment(),
                                            sentiment:sentiment
                                       }
                              
                                       tweets.push(item);

                                       count++       
                                                              
                                       if (count == numfiles) { //last element exit
                                           callback(tweets);
                                       }
                                        
                                   }catch(ex){
                                       console.log("=======ERROR=============");
                                       console.log(ex);
                                   }
                                });
                                 
                                
                                 
                            });  
                             
                       });  
                        
                    });
    
     
                });
                 
            });
          
        }); 
    })  
}


function runGlobal(numfiles,mainFolder,callback) {
    
    var tweets=[];
    var count = 0;
   crud.deleteGlobalTweet(() => {  
        
        fs.readdir(mainFolder, (err, days) => {         
            if (err) throw err;

            days.forEach(day => {
                
                fs.readdir(mainFolder  + "/" + day, (err, files) => {

                    files.forEach(file => { 

                        file =  mainFolder  + "/" + day + "/" + file;
                        
                        //console.log(file);
                        
                        fs.readFile(file, 'utf8', function (err, contents) {
                            if (err) throw err;
        
                            try  {
                                var tweet  = JSON.parse(contents);
                                var doc = lorca(tweet.full_text); // sentimental

                                var sentiment; 
                                        if (doc.sentiment() == 0) 
                                            sentiment = "Neutral";
                                        else if (doc.sentiment() < 0) 
                                            sentiment = "Negative";
                                        else if (doc.sentiment() > 0) 
                                            sentiment = "Positive";


                                item = {
                                    // id : tweet.id,
                                     created: day,
                                     type: "global",
                                     file : file,
                                     full_text : tweet.full_text,
                                     location : tweet.user.location,
                                     hashtags: tweet.entities.hashtags,
                                     lorca: doc.sentiment(),
                                     sentiment:sentiment
                                     
                                }
                        
                                tweets.push(item);

                                count++       
                        
                                if (count == numfiles) { //last element exit for
                                    callback(tweets);
                                }
                                
                            }catch(ex){
                                console.log("=======ERROR=============");
                                console.log(ex + " " + file);
                            }
                        });
                        
                    });  
                          
                             
                });  
                        
            });            
        });
        
     }); 
        
}



//GET NUMEBER OF FILES
countFiles("./tweets_local", function (err, results) {
    if (err) throw err;

    var numfiles = results.files -1;
    console.log("File local to import : " + numfiles);
    
    runLocal(numfiles,"./tweets_local",(tweets) =>  {

        countFiles("./tweets_global", function (err, results) {
            if (err) throw err;
        
            var numfiles = results.files -1;
            console.log("File global to import : " + numfiles);
            runGlobal(numfiles,"./tweets_global",(x) =>  {
    
                 var array_tweets = tweets.concat(x);  

                //console.log(array_tweets.length);
                
                //BULK INSERT 
                crud.InserTweets(array_tweets , (err, resp, status)=> {
                    if (err) throw err;

                    //console.log(resp.length);

                    for (var key in resp) {
                    //    console.log(resp[key]);
                    }; 
      
                    
                });
                 
                 
             });

        });
        
    
    });
})