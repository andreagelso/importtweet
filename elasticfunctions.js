
var client = require('./connection.js');

module.exports = {


    InserTweets: function (arrayobj, callback) {
        var items=[];

            
        arrayobj.forEach(element => {
            items.push({ index:  { _index: 'twitter', _type: 'twitter' , _id: element.file }},element);
        });


         client.bulk({body:items}, function (err, resp, status) {
            if (err) throw err;
            
            callback(err, resp, status);
        }); 
       
    },


    GetTweets: function (callback) {
        client.search({
            index: 'twitter',
            type: 'twitter',
            scroll : "1m", 
            size:10000,
            body: {
                query: {
                        "match_all": {} 
                }
            }
        }, function (err, resp, status) {
           
            callback(err,resp);
        });
    },


    GetRandomTweets: function (callback) {
        client.search({
            index: 'twitter',
            type: 'twitter',
            size:200,
            body: {
                "query": {
                  "function_score": {
                    "query": {
                      "match_all": {}
                    },
                    "functions": [
                      {
                        "random_score": {}
                      }
                    ]
                  }
                }
              }
            
        }, function (err, resp, status) {
            callback(err,resp);
        });
    },


    UpdateTweets: function (arrayobj, callback) {
        var items=[];

        arrayobj.forEach(item => {
            items.push({ update:  { _index: 'twitter', _type: 'twitter', _id: item._id }},
            {doc:item.element});
        });
 
        client.bulk({body:items}, function (err, resp, status) {
            callback(err, resp, status);
        });
    },
    
    deleteAll: function (callback) {
        
        client.indices.delete({
            index: 'twitter',
          
        }, function (err, res) {
            if (err) {
                console.error(err.message);
            }
            callback();
        });

    },


    deleteGlobalTweet: function (callback) {
      
        
        client.deleteByQuery({
            index: 'twitter',
            body: {
                query: {"match": {"type": "global"}}
            }
        }, function (err, res) {
            if (err)  
               console.error("DELETE" + err.message);
               
               callback();
        }); 
        

    },

    deleteLocalTweet: function (callback) {
         
         
        client.deleteByQuery({
            index: 'twitter',
            
            body: {
                query: {"match": {"type": "local"}}
            }
        }, function (err, res) {
            if (err)
                console.error(err);
            callback();
        });
         

    },


    printerrors(type, res) {
        console.log("========== ERROR SCHEMA: " + type + " ================")
        for (var key in res.errors) {
            console.log(res.errors[key].message);
        }
    }

};






